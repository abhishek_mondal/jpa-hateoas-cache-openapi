package com.hateoas.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

//@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Enrollment {

    private Long enrollId;
    private String bankName;
    private String bankIFSCCode;

}

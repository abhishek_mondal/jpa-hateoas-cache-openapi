package com.hateoas.test.service;

import com.hateoas.test.exception.CustomerNotFoundException;
import com.hateoas.test.model.Customer;
import com.hateoas.test.repository.CustomerRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer saveCustomer(@NonNull final Customer customer) {
        return customerRepository.save(customer);
    }

    public Iterable<Customer> saveAllCustomer(@NonNull final List<Customer> customers) {
        return customerRepository.saveAll(customers);
    }

    @Cacheable(
            value = "getCustomerDetailById",
            key = "#customerId",
            condition = "#customerId > 1")
    public Customer getCustomerDetail(Long customerId) {
        log.info("Entry into getCustomerDetail");
        Optional<Customer> customer = customerRepository.findById(customerId);

        if (!customer.isPresent()) {
            throw new CustomerNotFoundException("Customer not found for id :: " + customerId);
        }
        return customer.get();
    }

    public Iterable<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public void deleteCustomer(Long customerId) {
        customerRepository.deleteById(customerId);
    }

}

package com.hateoas.test.controller;

import com.hateoas.test.model.Customer;
import com.hateoas.test.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping(value = "/customer", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> saveCustomer(@Valid @RequestBody Customer customer) {
        Customer newCustomer = null;
        try {
            newCustomer = customerService.saveCustomer(customer);
            Link selfLink = WebMvcLinkBuilder.linkTo(CustomerController.class)
                    .slash("customer")
                    .slash(newCustomer.getCustomerId())
                    .withSelfRel();
            newCustomer.add(selfLink);

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return new ResponseEntity<>(newCustomer, HttpStatus.CREATED);
    }

    @PostMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<Customer>> saveAllCustomer(@Valid @RequestBody List<Customer> customers) {
        Iterable<Customer> newCustomers = null;
        try {
            newCustomers = customerService.saveAllCustomer(customers);
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return new ResponseEntity<>(newCustomers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/customer/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCustomerById(@Valid @PathVariable Long customerId) {
        Customer customer = customerService.getCustomerDetail(customerId);
        if(null != customer) {
            HttpHeaders headers = new HttpHeaders();
            headers.setCacheControl(CacheControl.noCache().getHeaderValue());
            headers.setPragma("no-cache");
            headers.setExpires(0L);
            return ResponseEntity.ok().headers(headers).body(customer);
        }
        return ResponseEntity.ok("Customer Not Found");
    }

    @GetMapping(value = "/customers", produces = {"application/json"})
    public CollectionModel<Customer> getAllCustomers() {
        List<Customer> allCustomers = (List<Customer>) customerService.getAllCustomers();
        for (Customer customer : allCustomers) {
            Long customerId = customer.getCustomerId();
            Link selfLink = WebMvcLinkBuilder.linkTo(CustomerController.class)
                    .slash("customer")
                    .slash(customerId)
                    .withSelfRel();
            customer.add(selfLink);
        }
        Link selfLink = WebMvcLinkBuilder.linkTo(CustomerController.class)
                .slash("customers")
                .withSelfRel();
        CollectionModel<Customer> result = CollectionModel.of(allCustomers, selfLink);
        // return new ResponseEntity<>(allCustomers, HttpStatus.OK);
        return result;
    }

    @PatchMapping("/customer/{customerId}")
    public ResponseEntity<?> updateCustomer(@RequestBody Customer customer, @PathVariable Long customerId) {
        Customer existingCustomer = customerService.getCustomerDetail(customerId);
        existingCustomer = mapToNewUpdate(existingCustomer, customer);

        customerService.saveCustomer(existingCustomer);

        return ResponseEntity.ok(existingCustomer);

    }

    @PutMapping("/customer/{customerId}")
    public ResponseEntity<?> replaceCustomer(@RequestBody Customer customer, @PathVariable Long customerId) {
        Customer existingCustomer = customerService.getCustomerDetail(customerId);

        existingCustomer.setFirstName(customer.getFirstName());
        existingCustomer.setLastName(customer.getLastName());
        existingCustomer.setAddress(customer.getAddress());

        customerService.saveCustomer(existingCustomer);

        return ResponseEntity.ok(existingCustomer);

    }

    @DeleteMapping("/customer/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long customerId) {
        Customer existingCustomer = customerService.getCustomerDetail(customerId);
        customerService.deleteCustomer(customerId);
        return ResponseEntity.ok("Customer deleted successfully");
    }

    private Customer mapToNewUpdate(Customer existingCustomer, Customer customer) {
        if (StringUtils.hasText(customer.getFirstName())) {
            existingCustomer.setFirstName(customer.getFirstName());
        }

        if (StringUtils.hasText(customer.getLastName())) {
            existingCustomer.setLastName(customer.getLastName());
        }

        if (StringUtils.hasText(customer.getAddress())) {
            existingCustomer.setAddress(customer.getAddress());
        }

        return existingCustomer;
    }


}
